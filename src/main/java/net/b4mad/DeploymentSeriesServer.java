package net.b4mad;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.CorsHandler;
import io.vertx.ext.web.handler.sockjs.BridgeOptions;
import io.vertx.ext.web.handler.sockjs.PermittedOptions;
import io.vertx.ext.web.handler.sockjs.SockJSHandler;
import io.vertx.kafka.client.consumer.KafkaConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

/**
 * A ...
 *
 * @author <a href="https://gitlab.com/goern">Christoph Görn</a>
 */
public class DeploymentSeriesServer extends AbstractVerticle {
  private static final Logger LOG = LoggerFactory.getLogger(DeploymentSeriesServer.class);
  private HashSet<String> projects = new HashSet<String>();
  private Map<String, JsonObject> builds = new HashMap<String, JsonObject>();

  @Override
  public void start(Future<Void> fut) {
    Router router = Router.router(vertx);

    JsonArray deployments = new JsonArray();

    BridgeOptions opts = new BridgeOptions().addOutboundPermitted(new PermittedOptions().setAddress("eventrouter"))
        .addInboundPermitted(new PermittedOptions().setAddress("eventrouter"))
        .addOutboundPermitted(new PermittedOptions().setAddress("projects"))
        .addInboundPermitted(new PermittedOptions().setAddress("projects"))
        .addOutboundPermitted(new PermittedOptions().setAddress("deployments"))
        .addInboundPermitted(new PermittedOptions().setAddress("deployments"))
        .addOutboundPermitted(new PermittedOptions().setAddress("builds"))
        .addInboundPermitted(new PermittedOptions().setAddress("builds"));

    SockJSHandler ebHandler = SockJSHandler.create(vertx).bridge(opts);
    router.route("/eventbus/*").handler(ebHandler);

    // router.route().handler(StaticHandler.create());

    router.route()
        .handler(CorsHandler.create("127\\.0\\.0\\.1\\.nip\\.io").allowedMethod(io.vertx.core.http.HttpMethod.GET) // TODO 12f should come from ENV
            .allowedMethod(io.vertx.core.http.HttpMethod.POST).allowedMethod(io.vertx.core.http.HttpMethod.OPTIONS)
            .allowCredentials(true).allowedHeader("Access-Control-Allow-Method")
            .allowedHeader("Access-Control-Allow-Origin").allowedHeader("Access-Control-Allow-Credentials")
            .allowedHeader("Content-Type"));

    vertx.createHttpServer().requestHandler(router::accept).listen(config().getInteger("http.port", 8080), result -> {
      if (result.succeeded()) {
        fut.complete();
      } else {
        fut.fail(result.cause());
      }
    });

    EventBus eb = vertx.eventBus();

    Map<String, String> config = new HashMap<>();
    config.put("bootstrap.servers", "kafka:9092");
    config.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
    config.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
    config.put("group.id", "eventbus");
    config.put("auto.offset.reset", "latest");
    config.put("enable.auto.commit", "false");

    // use consumer for interacting with Apache Kafka
    KafkaConsumer<String, String> projectsConsumer = KafkaConsumer.create(vertx, config);
    KafkaConsumer<String, String> deploymentsConsumer = KafkaConsumer.create(vertx, config);
    KafkaConsumer<String, String> buildsConsumer = KafkaConsumer.create(vertx, config);
    KafkaConsumer<String, String> gitHashConsumer = KafkaConsumer.create(vertx, config);

    projectsConsumer.handler(record -> {
      LOG.debug("Processing Project: key=" + record.key() + ",value=" + record.value() + ",partition="
          + record.partition() + ",offset=" + record.offset());

      projects.add(record.value());
    });

    deploymentsConsumer.handler(record -> {
      LOG.debug("Processing Deployment: key=" + record.key() + ",value=" + record.value() + ",partition="
          + record.partition() + ",offset=" + record.offset());

      JsonObject event = new JsonObject(record.value());
      JsonObject eventMetadata = event.getJsonObject("event").getJsonObject("metadata");

      deployments.add(eventMetadata);

    });

    buildsConsumer.handler(record -> {
      LOG.debug("Processing Build: key=" + record.key() + ",value=" + record.value() + ",partition="
          + record.partition() + ",offset=" + record.offset());

      JsonObject build = new JsonObject(record.value());
      String buildID = build.getString("uid");

      if (builds.containsKey(buildID)) {
        build.mergeIn(builds.get(buildID));
        builds.put(buildID, build);
      } else {
        builds.put(buildID, build);
      }
    });

    gitHashConsumer.handler(record -> {
      LOG.debug("Processing Build: key=" + record.key() + ",value=" + record.value() + ",partition="
          + record.partition() + ",offset=" + record.offset());

      JsonObject gitHashInfo = new JsonObject(record.value());
      String buildID = gitHashInfo.getString("uid");

      if (builds.containsKey(buildID)) {
        JsonObject build = builds.get(buildID);

        build.put("git_repository_url", gitHashInfo.getString("SourceRepository"));
        build.put("git_commit_hash", gitHashInfo.getString("SourceCommitHash"));

        builds.put(buildID, build);
      }
    });

    projectsConsumer.subscribe("projects", ar -> {
      if (ar.succeeded()) {
        System.out.println("Subscribed to Kafka Topic 'projects'");
      } else {
        System.out.println("Could not subscribe " + ar.cause().getMessage());
      }
    });

    deploymentsConsumer.subscribe("deployments", ar -> {
      if (ar.succeeded()) {
        System.out.println("Subscribed to Kafka Topic 'deployments'");
      } else {
        System.out.println("Could not subscribe " + ar.cause().getMessage());
      }
    });

    buildsConsumer.subscribe("builds", ar -> {
      if (ar.succeeded()) {
        System.out.println("Subscribed to Kafka Topic 'builds'");
      } else {
        System.out.println("Could not subscribe " + ar.cause().getMessage());
      }
    });

    gitHashConsumer.subscribe("openshift_builds_git_commit_hash", ar -> {
      if (ar.succeeded()) {
        System.out.println("Subscribed to Kafka Topic 'openshift_builds_git_commit_hash'");
      } else {
        System.out.println("Could not subscribe " + ar.cause().getMessage());
      }
    });

    vertx.setPeriodic(5000, t -> {
      JsonArray _projects = new JsonArray();
      JsonArray _builds = new JsonArray();

      Iterator<String> iterator = projects.iterator();
      while (iterator.hasNext()) {
        String name = iterator.next();
        JsonObject group = new JsonObject();
        group.put("id", name.replace("\"", ""));
        group.put("content", name.replace("\"", ""));

        _projects.add(group);
      }
      eb.send("projects", _projects);

      Iterator<Map.Entry<String, JsonObject>> _bIterator = builds.entrySet().iterator();

      while (_bIterator.hasNext()) {
        Map.Entry<String, JsonObject> entry = (Map.Entry<String, JsonObject>) _bIterator.next();
        JsonObject o = entry.getValue();

        if (o.getString("start") == null) {
          o.put("start", o.getString("created_at"));
        }

        o.put("content", o.getString("namespace") + "/" + o.getString("name"));
        o.put("group", o.getString("namespace"));

        _builds.add(o);
      }
      eb.send("builds", _builds);

      eb.send("deployments", deployments);
    });
  }
}