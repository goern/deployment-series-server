FROM fabric8/java-jboss-openjdk8-jdk

ENV JAVA_APP_JAR deployment-series-server-0.1.0-fat.jar 

ADD target/$JAVA_APP_JAR /deployments

EXPOSE 8080
