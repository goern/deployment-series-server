# Deploy

```
oc new-build --binary --name=deployment-series-server
mvn package -DskipTests
oc start-build deployment-series-server --from-dir=. --follow
oc new-app deployment-series-server
oc expose service deployment-series-server
```